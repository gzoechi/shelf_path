## 0.1.2

* increased upper bound on shelf

## 0.1.1+1

* bug fix in addPathParametersToContext

## 0.1.1

* deprecated some functions
* upgraded minimum shelf version requirement
* fixed bug due to immutable context

## 0.1.0

* First release