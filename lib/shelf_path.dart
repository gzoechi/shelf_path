// Copyright (c) 2014, The Shelf Path project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_path;

import 'package:shelf/shelf.dart';
import 'dart:collection';


const String _PATH_PARAMETERS_PARAM_NAME = 'shelf_path.parameters';

/// Fetches all the path parameters from the given [request]
Map<String, Object> getPathParameters(Request request) =>
    getPathParametersInContext(request.context);

/// Fetches all the path parameters from the given [context]
Map<String, Object> getPathParametersInContext(Map<String, Object> context) {
  final pathVars = context[_PATH_PARAMETERS_PARAM_NAME];
  return pathVars != null? pathVars : const {};
}

/// Fetches a single path parameter from the given [request] with the given
/// [parameterName]
Object getPathParameter(Request request, String parameterName) =>
    getPathParameters(request)[parameterName];

Request setPathParameters(Request request, Map<String, Object> pathParameters) {
  return request.change(context:
      { _PATH_PARAMETERS_PARAM_NAME: pathParameters });
}

Request addPathParameters(Request request, Map<String, Object> pathParameters) {
  return request.change(context:
      { _PATH_PARAMETERS_PARAM_NAME:
        _mergePathParameters(request.context, pathParameters) });
}

void setPathParametersInContext(Map<String, Object> context,
                                Map<String, Object> pathParameters) {
  context[_PATH_PARAMETERS_PARAM_NAME] = new UnmodifiableMapView(pathParameters);
}

void addPathParametersToContext(Map<String, Object> context,
                                Map<String, Object> pathParameters) {
  var newParams = _mergePathParameters(context, pathParameters);
  context[_PATH_PARAMETERS_PARAM_NAME] = new UnmodifiableMapView(newParams);
}

Map _mergePathParameters(Map<String, Object> context, Map<String, Object> pathParameters) {
  return {}
    ..addAll(getPathParametersInContext(context))
    ..addAll(pathParameters);
}

@deprecated
void setPathParameter(Request request, String parameterName, Object value) {
    getPathParameters(request)[parameterName] = value;
}


