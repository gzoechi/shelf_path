# Path Parameters for Dart Shelf

[![Pub Version](http://img.shields.io/pub/v/shelf_path.svg)](https://pub.dartlang.org/packages/shelf_path)

## Introduction

Provides a small library for [Shelf](https://api.dartlang.org/apidocs/channels/be/dartdoc-viewer/shelf) to decouple producers of `path parameters` such as *routers* from consumers like your own *handlers* or other middleware like *binding frameworks*.

This supports interoperability so that you can plug and play which middleware you like as long as they support *Dart Path*.

In a nutshell, *Shelf Path* encapsulates the storage of *path parameters* in the Shelf Request context property. It exposes two sets of functions:

 1. *Functions for consumers to __Read__ path properties*: `getPathParameters`, `getPathParameter`
 2. *Functions for producers to __Write__ path properties*: `setPathParameters`, `setPathParameter`, `addPathParameter`

## Example
Say we have an end point that serves up queries on a nomintated bank account and supports filtering by a minimum amount. The requests look like

```
GET /banking/account/123456/deposit?minAmount=20
```

One way to represent this is using the powerful [Uri Template](http://tools.ietf.org/html/rfc6570) standard which is supported by the very useful Dart [uri](http://pub.dartlang.org/packages/uri) package. It supports both segment parameters and query parameters.

For example

```
/banking/account/{accountId}/deposit{?minAmount}
```

which has two path variables:

 1. `accountId`
 2. `minAmount`
 
### Producer
Shelf Middleware such as routers can match the request path against this template and a map like

```
{
  'accountId': '123456',
  'minAmount': '20'
}
```

These path parameters can now be added to the context using the *Shelf Path* function *addPathParameters*

```
var pathParams = {
  'accountId': '123456',
  'minAmount': '20'
};

addPathParameters(pathParams);
```

### Consumer

An end user can now write a handler and extract the path parameters using *getPathParameter*

```
depositSearchHandler(request) {
  var accountId = getPathParameter(request, 'accountId');
  var minAmount = getPathParameter(request, 'minAmount');

  return new Response.ok("Querying deposits for account $accountId for amounts >= $minAmount");
}  
```

## Compatible Middleware
### Producers

 - [Shelf Route](https://pub.dartlang.org/packages/shelf_route)

### Consumers

 - [Shelf Bind](https://pub.dartlang.org/packages/shelf_bind)

